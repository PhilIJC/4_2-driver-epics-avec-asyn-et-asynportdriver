# 4_2 Driver EPICS avec asyn et asynPortDriver

> 90 mn

## Table des matières

<!-- vim-markdown-toc GitLab -->

* [Prérequis](#prérequis)
* [Références](#références)
* [Présentation](#présentation)
* [TP](#tp)
    * [Premier petit driver simple](#premier-petit-driver-simple)
    * [Premier exercice](#premier-exercice)
    * [Second petit driver moins simple](#second-petit-driver-moins-simple)
    * [Second exercice](#second-exercice)

<!-- vim-markdown-toc -->

## Prérequis

- [Rocky Linux 8](https://rockylinux.org/)

## Références

- <https://controlssoftware.sns.ornl.gov/training/>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/#presentations>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/18_Asyn.pdf>

- <https://controlssoftware.sns.ornl.gov/training/2019_USPAS/#presentations>
- <https://controlssoftware.sns.ornl.gov/training/2019_USPAS/Presentations/17%20ASYN.pdf>

- <https://epics.anl.gov/>
- <https://epics.anl.gov/docs/>
- <https://epics.anl.gov/docs/training.php>
- <https://epics.anl.gov/docs/APS2015.php>

- <https://epics.anl.gov/docs/APS2015/06-AsynDriver.pdf>
- <https://epics.anl.gov/docs/APS2015/06-AsynDriver.m4v>

- <https://epics.anl.gov/docs/APS2015/07-AsynPortDriver.pdf>
- <https://epics.anl.gov/docs/APS2015/07-AsynPortDriver.m4v>

- <https://epics.anl.gov/docs/APS2015/08-AsynDriver-Tutorial.pdf>
- <https://epics.anl.gov/docs/APS2015/08-AsynDriver-Tutorial.m4v>
- <https://epics.anl.gov/docs/APS2015/09-AsynDriver-Tutorial-Code.pdf>

- <https://epics.anl.gov/docs/APS2015/10-AsynDriver-Lab.pdf>

## Présentation

> 45 mn

Cf. [présentation asynPortDriver](./presentation_asynportdriver.pdf).

## TP

> 45 mn

### Premier petit driver simple

L'objectif ici va être de créer un programme pilotant un signal qui va "ramp" (grimper) en dents de
scie.

- Créez un top :
    ```console
    $ cd $HOME
    $ mkdir -p Tops/asyndemo-top
    $ cd Tops/asyndemo-top
    $ makeBaseApp.pl -a linux-x86_64 -t ioc asyndemo && makeBaseApp.pl -a linux-x86_64 -i -t ioc -p asyndemo asyndemo
    $ make
    ```

- Importez le module ASYN dans votre top :
    ```console
    $ vi configure/RELEASE
        > # RELEASE - Location of external support modules
        > #
        > # IF YOU CHANGE ANY PATHS in this file or make API changes to
        > # any modules it refers to, you should do a "make rebuild" in
        > # this application's top level directory.
        > #
        > # The EPICS build process does not check dependencies against
        > # any files from outside the application, so it is safest to
        > # rebuild it completely if any modules it depends on change.
        > #
        > # Host- or target-specific settings can be given in files named
        > #  RELEASE.$(EPICS_HOST_ARCH).Common
        > #  RELEASE.Common.$(T_A)
        > #  RELEASE.$(EPICS_HOST_ARCH).$(T_A)
        > #
        > # This file is parsed by both GNUmake and an EPICS Perl script,
        > # so it may ONLY contain definititions of paths to other support
        > # modules, variable definitions that are used in module paths,
        > # and include statements that pull in other RELEASE files.
        > # Variables may be used before their values have been set.
        > # Build variables that are NOT used in paths should be set in
        > # the CONFIG_SITE file.
        >
        > # Variables and paths to dependent modules:
        > #MODULES = /path/to/modules
        > #MYMODULE = $(MODULES)/my-module
        >
        > # If using the sequencer, point SNCSEQ at its top directory:
        > #SNCSEQ = $(MODULES)/seq-ver
      + >
      + > ASYN = /opt/epics/support/asyn-R4-42
        >
        > # EPICS_BASE should appear last so earlier modules can override stuff:
        > EPICS_BASE = /opt/epics/base-7.0-gcc
        >
        > # Set RULES here if you want to use build rules from somewhere
        > # other than EPICS_BASE:
        > #RULES = $(MODULES)/build-rules
        >
        > # These lines allow developers to override these RELEASE settings
        > # without having to modify this file directly.
        > -include $(TOP)/../RELEASE.local
        > -include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local
        > -include $(TOP)/configure/RELEASE.local
    ```

- Créez deux records associés au signal :
    ```console
    $ vi asyndemoApp/Db/asyndemo.db
      + > # Valeur du signal qui sera piloté par ASYN
      + > record(ai, "$(P)$(R)Value")
      + > {
      + >    field(PINI, "1")
      + >    field(DTYP, "asynFloat64")
      + >    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))VALUE")
      + >    field(PREC, "5")
      + >    field(SCAN, "I/O Intr")
      + > }
      + >
      + > # Limite suppérieure du signal
      + > record(ao, "$(P)$(R)Range")
      + > {
      + >    field(PINI, "1")
      + >    field(DTYP, "asynFloat64")
      + >    field(OUT,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))RANGE")
      + >    field(PREC, "5")
      + > }

    $ vi asyndemoApp/Db/Makefile
        > TOP=../..
        > include $(TOP)/configure/CONFIG
        > #----------------------------------------
        > #  ADD MACRO DEFINITIONS AFTER THIS LINE
        >
        > #----------------------------------------------------
        > # Create and install (or just install) into <top>/db
        > # databases, templates, substitutions like this
        > #DB += xxx.db
      + > DB += asyndemo.db
        >
        > #----------------------------------------------------
        > # If <anyname>.db template is not named <anyname>*.template add
        > # <anyname>_template = <templatename>
        >
        > include $(TOP)/configure/RULES
        > #----------------------------------------
        > #  ADD RULES AFTER THIS LINE
        >

    $ make clean && make && echo OK || echo KO
    ```

- Créez le fichier `asyndemoApp/src/asyndemo.cpp` avec le code asyn contenu dans le fichier
  [`./src/asyndemo.cpp`](./src/asyndemo.cpp) (⚠️ prenez bien le temps de lire son contenu et de
  vous familiariser avec ⚠️).

- Créez le `.dbd` permettant de définir / d'appeler notre code asyn côté EPICS
    ```console
    $ vi asyndemoApp/src/asyndemoInclude.dbd
      + > include "base.dbd"
      + > include "asyn.dbd"
      + > registrar("asyndemoRegister")
    ```

- Incluez votre code C++ et sa dépendance ASYN dans le Makefile associé :
    ```console
    $ vi asyndemoApp/src/Makefile
        > TOP=../..
        >
        > include $(TOP)/configure/CONFIG
        > #----------------------------------------
        > #  ADD MACRO DEFINITIONS AFTER THIS LINE
        > #=============================
        >
        > #=============================
        > # Build the IOC application
        >
        > PROD_IOC = asyndemo
        > # asyndemo.dbd will be created and installed
        > DBD += asyndemo.dbd
        >
        > # asyndemo.dbd will be made up from these files:
        > asyndemo_DBD += base.dbd
        >
        > # Include dbd files from all support applications:
        > #asyndemo_DBD += xxx.dbd
        >
        > # Add all the support libraries needed by this IOC
        > #asyndemo_LIBS += xxx
      + > asyndemo_LIBS += asyn
      + >
      + > # Add asyn cpp application
      + > asyndemo_SRCS += asyndemo.cpp
        >
        > # asyndemo_registerRecordDeviceDriver.cpp derives from asyndemo.dbd
        > asyndemo_SRCS += asyndemo_registerRecordDeviceDriver.cpp
        >
        > # Build the main IOC entry point on workstation OSs.
        > asyndemo_SRCS_DEFAULT += asyndemoMain.cpp
        > asyndemo_SRCS_vxWorks += -nil-
        >
        > # Add support from base/src/vxWorks if needed
        > #asyndemo_OBJS_vxWorks += $(EPICS_BASE_BIN)/vxComLibrary
        >
        > # Finally link to the EPICS Base libraries
        > asyndemo_LIBS += $(EPICS_BASE_IOC_LIBS)
        >
        > #===========================
        >
        > include $(TOP)/configure/RULES
        > #----------------------------------------
        > #  ADD RULES AFTER THIS LINE

    $ make clean && make && echo OK || echo KO
    ```

- Créez enfin, le fichier `.cmd` permettant de démarrer votre application :
    ```console
    $ vi iocBoot/iocasyndemo/st.cmd
        > #! ../../bin/linux-x86_64/asyndemo
      - >
      - > #- You may have to change asyndemo to something else
      - > #- everywhere it appears in this file
        >
        > < envPaths
        >
        > cd "${TOP}"
        >
        > ## Register all support components
        > dbLoadDatabase "dbd/asyndemo.dbd"
        > asyndemo_registerRecordDeviceDriver pdbbase
      - >
      - > ## Load record instances
      - > #dbLoadRecords("db/xxx.db","user=stephane")
        >
      + > # Optionally turn on asynTraceFlow and asynTraceError for global trace, i.e. for tracing connection issues
      + > #asynSetTraceMask("", 0, 17)
      + >
      + > asyndemoConfigure("dev1")
      + >
      + > # Optionally load asyn debug related PVs
      + > #cd "${ASYN}"
      + > #dbLoadRecords("db/asynRecord.db","P=demo:,R=1,PORT=dev1,ADDR=0,OMAX=80,IMAX=80")
      + >
      + > cd "${TOP}"
      + > dbLoadRecords("db/asyndemo.db","P=demo:,R=1:,PORT=dev1,ADDR=0,TIMEOUT=1")
      + > #asynSetTraceMask("dev1",0,0xff)
      + > asynSetTraceIOMask("dev1",0,0x2)
        >
        > cd "${TOP}/iocBoot/${IOC}"
        > iocInit()
        >
      - > ## Start any sequence programs
      - > #seq sncxxx,"user=stephane"

    $ chmod +x iocBoot/iocasyndemo/st.cmd
    ```

- Vous pouvez enfin démarrer votre IOC pour observer votre driver en action :
    ```console
    $ cd iocBoot/iocasyndemo
    $ ./st.cmd
        #!../../bin/linux-x86_64/asyndemo
        < envPaths
        ...
        iocInit
        Starting iocInit
        ############################################################################
        ## EPICS R7.0.6.2-DEV
        ## Rev. R7.0.6.1-149-gd82ab819ef751f5c1f0c
        ############################################################################
        iocRun: All initialization complete
        epics> dbl
        demo:1:Value
        demo:1:Range
        demo:1
        epics> asynReport 10
        dev1 multiDevice:No canBlock:No autoConnect:Yes
            enabled:Yes connected:Yes numberConnects 1
            nDevices 0 nQueued 0 blocked:No
            asynManagerLock:No synchronousLock:No
            exceptionActive:No exceptionUsers 0 exceptionNotifys 0
            traceMask:0x1 traceIOMask:0x2 traceInfoMask:0x1
            interfaceList
                asynCommon pinterface 0x7f04f4cf0cf0 drvPvt 0x563176889500
                asynDrvUser pinterface 0x7f04f4cf0a80 drvPvt 0x563176889500
                asynFloat64 pinterface 0x7f04f4cf0c20 drvPvt 0x563176889500
        Port: dev1
          Timestamp: 2022/09/28 17:39:58.883
        Parameter list 0
        Number of parameters is: 2
        Parameter 0 type=asynFloat64, name=VALUE, value=10, status=0
        Parameter 1 type=asynFloat64, name=RANGE, value=10, status=0
            float64 callback client address=0x7f04f4caff50, addr=-1, reason=0, userPvt=0x5631769595d0
    ```

- Dans un autre terminal vous pouvez un peu interagir avec l'IOC :
    ```console
    $ caget demo:1:Range
    demo:1:Range                   10

    $ camonitor demo:1:Value
    demo:1:Value                   2022-09-26 16:05:47.846397 0
    demo:1:Value                   2022-09-26 16:05:52.846473 1
    demo:1:Value                   2022-09-26 16:05:57.846647 2
    demo:1:Value                   2022-09-26 16:06:02.846850 3
    demo:1:Value                   2022-09-26 16:06:07.846961 4
    demo:1:Value                   2022-09-26 16:06:12.847143 5
    demo:1:Value                   2022-09-26 16:06:17.847290 6
    demo:1:Value                   2022-09-26 16:06:22.847397 7
    demo:1:Value                   2022-09-26 16:06:27.847550 8
    demo:1:Value                   2022-09-26 16:06:32.847721 9
    demo:1:Value                   2022-09-26 16:06:37.848032 10
    demo:1:Value                   2022-09-26 16:06:42.847927 0
    ```

- Puis vous pouvez démarrer Phoebus en lançant la commande `phoebus`, et ouvrir la vue associée en
  cliquant sur `File` → `Open...` →
  `path/to/4_2-driver-epics-avec-asyn-et-asynportdriver/ihm/main.bob`

### Premier exercice

Ajoutez un paramètre permettant de configurer - à la volée - la durer entre chaque incrémentation
de la PV `demo:1:Value`. Actuellement il faut attendre 5 secondes entre chaque incrémentation,
c'est trop long.

Pistes :
- Il vous faudra créer un nouveau record pour pouvoir écrire dessus (indice : le record peut être
  de type `ao`).
- Il vous faudra aussi modifier le fichier `asyndemoApp/src/asyndemo.cpp` pour y implémenter le
  code permettant d'intégrer le nouveau record à surveiller (indice : prenez exemple sur comment
  ont été intégrés les records "$(P)$(R)Value" et "$(P)$(R)Range").

À la fin vous devriez obtenir un résultat similaire à celui-ci :
```console
$ camonitor demo:1:Value # bouge toutes les 5 secondes
demo:1:Value                   2022-09-29 09:45:22.307185 0
demo:1:Value                   2022-09-29 09:45:27.307375 1
demo:1:Value                   2022-09-29 09:45:32.307440 2
demo:1:Value                   2022-09-29 09:45:37.307555 3

$ caput demo:1:Delai 1 # ici la PV pour modifier le temps entre chaque incrémentation a été appelée "Delai"
Old : demo:1:Delai                   5
New : demo:1:Delai                   1

$ camonitor demo:1:Value # bouge toutes les 1 seconde
demo:1:Value                   2022-09-29 09:47:12.312402 0
demo:1:Value                   2022-09-29 09:47:13.312707 1
demo:1:Value                   2022-09-29 09:47:14.313047 2
demo:1:Value                   2022-09-29 09:47:15.313380 3
```

### Second petit driver moins simple

- Dans un terminal lancez :
    ```console
    $ python path/to/4_2-driver-epics-avec-asyn-et-asynportdriver/src/server.py
    ```

- Si vous regardez le contenu de [`src/server.py`](./src/server.py), alors vous remarquerez qu'il
  s'agit d'un petit programme python implémentant un serveur TCP très basique permettant de piloter
  une variable située sur ce serveur. Ce driver va en fait essayer de se connecter à ce serveur et
  de commander la variable associée, comme s'il s'agissait d'un équipement branché en réseau (par
  exemple une alimentation dont on souhaiterait contrôler la tension).

- Dans un autre terminal lancez `telnet` pour tester les interactions avec le petit serveur python :
    ```console
    $ telnet 127.0.0.1 4242

    Trying 127.0.0.1...
    Connected to 127.0.0.1.
    Escape character is '^]'.

    ?
    test
    ```
    Appuyez sur `Ctrl+C` pour stopper `telnet`, idem pour stopper le serveur python.

- **L'objectif ici va donc être de créer un driver pilotant la variable se trouvant sur le petit
  serveur python présenté ci-dessus.**

- Commencez par créer un top :
    ```console
    $ cd $HOME
    $ mkdir -p Tops/asyndemo-with-python-server-top
    $ cd Tops/asyndemo-with-python-server-top
    $ makeBaseApp.pl -a linux-x86_64 -t ioc asyndemoserver && makeBaseApp.pl -a linux-x86_64 -i -t ioc -p asyndemoserver asyndemoserver
    $ make
    ```

- Importez le module ASYN dans votre top :
    ```console
    $ vi configure/RELEASE
        > # RELEASE - Location of external support modules
        > #
        > # IF YOU CHANGE ANY PATHS in this file or make API changes to
        > # any modules it refers to, you should do a "make rebuild" in
        > # this application's top level directory.
        > #
        > # The EPICS build process does not check dependencies against
        > # any files from outside the application, so it is safest to
        > # rebuild it completely if any modules it depends on change.
        > #
        > # Host- or target-specific settings can be given in files named
        > #  RELEASE.$(EPICS_HOST_ARCH).Common
        > #  RELEASE.Common.$(T_A)
        > #  RELEASE.$(EPICS_HOST_ARCH).$(T_A)
        > #
        > # This file is parsed by both GNUmake and an EPICS Perl script,
        > # so it may ONLY contain definititions of paths to other support
        > # modules, variable definitions that are used in module paths,
        > # and include statements that pull in other RELEASE files.
        > # Variables may be used before their values have been set.
        > # Build variables that are NOT used in paths should be set in
        > # the CONFIG_SITE file.
        >
        > # Variables and paths to dependent modules:
        > #MODULES = /path/to/modules
        > #MYMODULE = $(MODULES)/my-module
        >
        > # If using the sequencer, point SNCSEQ at its top directory:
        > #SNCSEQ = $(MODULES)/seq-ver
      + >
      + > ASYN = /opt/epics/support/asyn-R4-42
        >
        > # EPICS_BASE should appear last so earlier modules can override stuff:
        > EPICS_BASE = /opt/epics/base-7.0-gcc
        >
        > # Set RULES here if you want to use build rules from somewhere
        > # other than EPICS_BASE:
        > #RULES = $(MODULES)/build-rules
        >
        > # These lines allow developers to override these RELEASE settings
        > # without having to modify this file directly.
        > -include $(TOP)/../RELEASE.local
        > -include $(TOP)/../RELEASE.$(EPICS_HOST_ARCH).local
        > -include $(TOP)/configure/RELEASE.local
    ```

- Créez un record associé à la variable côté serveur :
    ```console
    $ vi asyndemoserverApp/Db/asyndemoserver.db
      + > record(stringin, "$(P)$(R)Server_PV_RBV")
      + > {
      + >    field(PINI, "1")
      + >    field(DTYP, "asynOctetRead")
      + >    field(INP,  "@asyn($(PORT),$(ADDR),$(TIMEOUT))SERVER_PV_RBV")
      + >    field(SCAN, "I/O Intr")
      + > }

    $ vi asyndemoserverApp/Db/Makefile
        > TOP=../..
        > include $(TOP)/configure/CONFIG
        > #----------------------------------------
        > #  ADD MACRO DEFINITIONS AFTER THIS LINE
        >
        > #----------------------------------------------------
        > # Create and install (or just install) into <top>/db
        > # databases, templates, substitutions like this
        > #DB += xxx.db
      + > DB += asyndemoserver.db
        >
        > #----------------------------------------------------
        > # If <anyname>.db template is not named <anyname>*.template add
        > # <anyname>_template = <templatename>
        >
        > include $(TOP)/configure/RULES
        > #----------------------------------------
        > #  ADD RULES AFTER THIS LINE
        >

    $ make clean && make && echo OK || echo KO
    ```

- Créez le fichier `asyndemoserverApp/src/asyndemoserver.cpp` avec le code asyn contenu dans le
  fichier [`./src/asyndemoserver.cpp`](./src/asyndemoserver.cpp) (⚠️ prenez bien le temps de lire
  son contenu et de vous familiariser avec ⚠️).

- Créez le `.dbd` permettant de définir / d'appeler notre code asyn côté EPICS
    ```console
    $ vi asyndemoserverApp/src/asyndemoserverInclude.dbd
      + > include "base.dbd"
      + > include "asyn.dbd"
      + > include "drvAsynIPPort.dbd"
      + > registrar("asyndemoserverRegister")
    ```

- Incluez votre code C++ et sa dépendance ASYN dans le Makefile associé :
    ```console
    $ vi asyndemoserverApp/src/Makefile
        > TOP=../..
        >
        > include $(TOP)/configure/CONFIG
        > #----------------------------------------
        > #  ADD MACRO DEFINITIONS AFTER THIS LINE
        > #=============================
        >
        > #=============================
        > # Build the IOC application
        >
        > PROD_IOC = asyndemoserver
        > # asyndemoserver.dbd will be created and installed
        > DBD += asyndemoserver.dbd
        >
        > # asyndemoserver.dbd will be made up from these files:
        > asyndemoserver_DBD += base.dbd
        >
        > # Include dbd files from all support applications:
        > #asyndemoserver_DBD += xxx.dbd
        >
        > # Add all the support libraries needed by this IOC
        > #asyndemoserver_LIBS += xxx
      + > asyndemoserver_LIBS += asyn
      + >
      + > # Add asyn cpp application
      + > asyndemoserver_SRCS += asyndemoserver.cpp
        >
        > # asyndemoserver_registerRecordDeviceDriver.cpp derives from asyndemoserver.dbd
        > asyndemoserver_SRCS += asyndemoserver_registerRecordDeviceDriver.cpp
        >
        > # Build the main IOC entry point on workstation OSs.
        > asyndemoserver_SRCS_DEFAULT += asyndemoserverMain.cpp
        > asyndemoserver_SRCS_vxWorks += -nil-
        >
        > # Add support from base/src/vxWorks if needed
        > #asyndemoserver_OBJS_vxWorks += $(EPICS_BASE_BIN)/vxComLibrary
        >
        > # Finally link to the EPICS Base libraries
        > asyndemoserver_LIBS += $(EPICS_BASE_IOC_LIBS)
        >
        > #===========================
        >
        > include $(TOP)/configure/RULES
        > #----------------------------------------
        > #  ADD RULES AFTER THIS LINE

    $ make clean && make && echo OK || echo KO
    ```

- Créez enfin le `.cmd` permettant de démarrer votre application :
    ```console
    $ vi iocBoot/iocasyndemoserver/st.cmd
        > #!../../bin/linux-x86_64/asyndemoserver
      - >
      - > #- You may have to change asyndemo to something else
      - > #- everywhere it appears in this file
        >
        > < envPaths
        >
        > cd "${TOP}"
        >
        > ## Register all support components
        > dbLoadDatabase "dbd/asyndemoserver.dbd"
        > asyndemoserver_registerRecordDeviceDriver pdbbase
      - >
      - > ## Load record instances
      - > #dbLoadRecords("db/xxx.db","user=stephane")
      + >
      + > # Optionally turn on asynTraceFlow and asynTraceError for global trace, i.e. no connected asynUser
      + > #asynSetTraceMask("", 0, 17)
      + >
      + > asyndemoserverConfigure("demoserverport", "localhost:4242");
      + >
      + > # Optionally load asyn debug related PVs
      + > #cd "${ASYN}"
      + > #dbLoadRecords("db/asynRecord.db","P=demo:,R=1,PORT=demoserverport,ADDR=0,OMAX=80,IMAX=80")
      + >
      + > cd "${TOP}"
      + > dbLoadRecords("db/asyndemoserver.db","P=asyn:,R=demoserver:,PORT=demoserverport,ADDR=0,TIMEOUT=1")
      + > #asynSetTraceMask("dev1",0,0xff)
      + > asynSetTraceIOMask("demoserverport",0,0x2)
        >
        > cd "${TOP}/iocBoot/${IOC}"
        > iocInit
      - >
      - > ## Start any sequence programs
      - > #seq sncxxx,"user=stephane"

    $ chmod +x iocBoot/iocasyndemoserver/st.cmd
    ```

- À présent, vous pouvez démarrer le serveur python :
    ```console
    $ python path/to/4_2-driver-epics-avec-asyn-et-asynportdriver/src/server.py
    ```
    Puis, dans un autre terminal, vous pouvez lancer le driver (⚠️ n'oubliez pas de (re)démarrer le
    serveur python, comme décris ci-dessus, avant chaque lancement du driver ⚠️):
    ```console
    $ cd $HOME/Tops/asyndemo-with-python-server-top
    $ cd iocBoot/iocasyndemoserver
    $ ./st.cmd
        #!../../bin/linux-x86_64/asyndemoserver
        ...
        iocInit
        Starting iocInit
        ############################################################################
        ## EPICS R7.0.6.2-DEV
        ## Rev. R7.0.6.1-149-gd82ab819ef751f5c1f0c
        ############################################################################
        iocRun: All initialization complete
        epics> dbl
        asyn:demoserver:Server_PV_RBV
        epics>
    ```

- Dans un autre terminal vous pouvez vérifier que vous lisez bien le contenu de la variable côté
  serveur :
    ```console
    $ caget asyn:demoserver:Server_PV_RBV
    asyn:demoserver:Server_PV_RBV plip
    ```

### Second exercice

Actuellement on récupère une variable sur le réseau, seulement pour la lire et mettre régulièrement
à jour la PV associée côté IOC. À présent je vous propose d'essayer d'implémenter le code
nécessaire pour permettre d'écrire dessus.

Pistes :
- Il vous faudra créer un nouveau record pour pouvoir écrire dessus (indice : le record peut être
  de type `stringout`).
- Il vous faudra implémenter une méthode d'asynPortDriver (indice : le nom de cette méthode
  commence par `write`, voir
  <https://epics.anl.gov/modules/soft/asyn/R4-38/asynDoxygenHTML/classasyn_port_driver.html>).

À la fin vous devriez obtenir un résultat similaire à celui-ci :
```console
$ caget asyn:demoserver:Server_PV # ici "asyn:demoserver:Server_PV" est la PV permettant d'écrire sur la variable côté serveur (initiallement elle doit être vide)
asyn:demoserver:Server_PV

$ caget asyn:demoserver:Server_PV_RBV
asyn:demoserver:Server_PV_RBV plip

$ caput asyn:demoserver:Server_PV "plop"
Old : asyn:demoserver:Server_PV
New : asyn:demoserver:Server_PV      plop

$ caget asyn:demoserver:Server_PV_RBV
asyn:demoserver:Server_PV plop
```

Exemples pratiques :
- <https://github.com/epics-modules/asyn>, dans les dossier `*App` commençant par `test*`, et dans
  les dossiers `ioc*` associés situés dans `iocBoot`.
- <https://github.com/epics-modules/quadEM>, dans le dossier `quadEMApp` et dans les dossiers
  `ioc*` associés situés dans `iocBoot`.

