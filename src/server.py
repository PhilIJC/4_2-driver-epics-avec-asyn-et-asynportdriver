import socket
import sys
import time

# Define a process variable
process_variable = "plip"

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind the socket to the port
bind_ip = "127.0.0.1"
bind_port = 4242
server_address = (bind_ip, bind_port)
print(f'starting up {server_address}', file=sys.stderr)
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)

while True:
    time.sleep(0.1)

    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()

    try:
        # Connection established
        print(f'connection from {client_address}')
        print("")

        # Receive the data in small chunks and retransmitit
        while True:
            time.sleep(0.1)
            data = connection.recv(16)
            data_str = data.decode('ascii')
            data_str = data_str.replace('\r','').replace('\n','')

            if data_str == "":
                pass
            elif data_str == "?":
                print(f'received {data} (to string = \'{data_str}\')')
                print(f'sending process variable content (i.e. {process_variable} to the client)')
                data_to_send = process_variable.encode('ascii')
                connection.sendall(data_to_send)
            elif data_str == "close!!!":
                print(f'received {data} (to string = \'{data_str}\')')
                break
            else:
                print(f'received {data} (to string = \'{data_str}\')')
                print(f'updating process variable content with received data (i.e. with \'{data_str}\')')
                process_variable = data_str
                data_to_send = process_variable.encode('ascii')
                connection.sendall(data_to_send)

    finally:
        # Clean up the connection
        connection.close()
        break


