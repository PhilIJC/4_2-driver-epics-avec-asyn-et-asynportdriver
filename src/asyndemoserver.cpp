/*
 * Exemple un peu plus concret de driver qui va récupérer une variable sur le réseau.
 * Ici il s'agit seulement de lire la variable et de la mettre à jour régulièrement côté IOC.
 */

//=================================================================================================
// Includes
//
#include <string.h>

#include <asynOctetSyncIO.h>
#include <asynPortDriver.h>
#include <drvAsynIPPort.h>

//=================================================================================================
// Parameters names
//
#define P_Server_PV_RBV_String    "SERVER_PV_RBV"

//=================================================================================================
// asynPortDriver based class
//
class asyndemoserver : public asynPortDriver
{
public:
    asyndemoserver(const char *portName, const char *IPPortName);

private:
    // Name of the current driver (used for logging purposes)
    const char* driverName = "asyndemoserver";

    // Indices used within asyn to identify a port parameter
    int P_Server_PV_RBV;

    // Asyn user used to specify de connection parameters
    asynUser *pasynUserIPPort_;

    // Background thread which does the actual work
    void pollerTask();

    // Wrapper function declaration (needed around the background thread)
    static void pollerTask_wrapper(void *arg);
};

//=================================================================================================
// asynPortDriver constructor
//
asyndemoserver::asyndemoserver(const char *portName, const char* IPPortName)
   : asynPortDriver(portName, // The name of the asyn port driver to be created
                    1, // How many addresses are supported on this port?
                    asynOctetMask | asynDrvUserMask, // Supported 'Interfaces' (types)
                    asynOctetMask,  // Interrupt mask, types that support I/O Intr
                    0, // ASYN_.. flags.  This driver does not block and it is not multi-device
                    1, // Autoconnect
                    0, // Default priority
                    0) // Default stack size
{
    // Name of the current function (used for logging purposes)
    const char* functionName = "asyndemoserver";

    // Register one or more parameters for this 'port'
    createParam(P_Server_PV_RBV_String, asynParamOctet, &P_Server_PV_RBV);

    // Initialize status report
    asynStatus status;

    // Configure connection with server
    char tcpPortName[255];
    strcpy(tcpPortName, "TCP_");
    strcat(tcpPortName, portName);
    status = (asynStatus)drvAsynIPPortConfigure(tcpPortName, IPPortName, 0, 0, 1);

    // Check that connection configuration succeded
    if (status) {
        asynPrint(pasynUserSelf,
                  ASYN_TRACE_ERROR,
                  "%s::%s error calling drvAsynIPPortConfigure,  port=%s, IP=%s, status=%d\n",
                  driverName,
                  functionName,
                  portName,
                  pasynUserIPPort_,
                  status);
    }

    // Connect to the server
    status = pasynOctetSyncIO->connect(tcpPortName, 0, &pasynUserIPPort_, NULL);

    // Check that server connection succeded
    if (status) {
        asynPrint(pasynUserSelf,
                  ASYN_TRACE_ERROR,
                  "[ASYN ERROR] %s:%s: pasynOctetSyncIO->connect failure, status=%d, IPPortName=%s, error message=%s\n",
                  driverName,
                  functionName,
                  status,
                  IPPortName,
                  pasynUserIPPort_->errorMessage);
    }

    // Create a thread that read and store server data
    status = (asynStatus)(epicsThreadCreate("asyndemoserverTask",
                          epicsThreadPriorityMedium,
                          epicsThreadGetStackSize(epicsThreadStackMedium),
                          pollerTask_wrapper, this) == NULL);

    // Check that thread creation succeded
    if (status) {
        asynPrint(pasynUserSelf,
                  ASYN_TRACE_ERROR,
                  "[ASYN ERROR] %s:%s: epicsThreadCreate failure, status=%d\n",
                  driverName,
                  functionName,
                  status);
    }
}

/* epicsThreadCreate(.., code, arg) needs a C function,
 * so pass this one which then calls the desired C++ method
 */
void asyndemoserver::pollerTask_wrapper(void *arg)
{
    asyndemoserver *self = (asyndemoserver *)arg;
    self->pollerTask();
}

//=================================================================================================
// Background thread which does the actual work
//
void asyndemoserver::pollerTask()
{
    // Name of the current function (used for logging purposes)
    static const char *functionName = "pollerTask";

    // Initialize status report
    asynStatus status;

    // Initialize communication related variables
    static const int MAX_RESPONSE_LEN = 16;
    char response[MAX_RESPONSE_LEN] = "";
    char msgToServer[16] = "?";
    size_t numWrite;
    size_t numRead;
    int eomReason;
    int isConnected;
    double timeout = 1.0;

    // Temporization / time delay (in seconds), between each iteration of the below loop
    static const int POLLER_PERIOD = 1;

    // Loop forever
    while (1) {
        lock();

        // Check connection status
        status = pasynManager->isConnected(pasynUserIPPort_, &isConnected);
        if (status) {
            asynPrint(pasynUserSelf,
                      ASYN_TRACE_ERROR,
                      "%s:%s: error calling pasynManager->isConnected, status=%d, error=%s\n",
                      driverName,
                      functionName,
                      status,
                      pasynUserIPPort_->errorMessage);
        }
        asynPrint(pasynUserSelf,
                  ASYN_TRACEIO_DRIVER,
                  "%s:%s: isConnected = %d\n",
                  driverName,
                  functionName,
                  isConnected);

        // Send a msg to the server and read its response (write/read operation)
        status = pasynOctetSyncIO->writeRead(pasynUserIPPort_,
                                             msgToServer,
                                             strlen(msgToServer),
                                             response,
                                             sizeof(response),
                                             timeout,
                                             &numWrite,
                                             &numRead,
                                             &eomReason);

        //asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
        //    "%s:%s: response=%s, status=%d\n",
        //    driverName, functionName, response, status);

        // Check the write/read operation status
        if (status) {
            asynPrint(pasynUserSelf,
                      ASYN_TRACE_ERROR,
                      "%s:%s: error calling pasynOctetSyncIO->writeRead, status=%d, error=%s\n",
                      driverName,
                      functionName,
                      status,
                      pasynUserIPPort_->errorMessage);
        }
        else {
            asynPrint(pasynUserSelf,
                      ASYN_TRACEIO_DRIVER,
                      "%s:%s: numWrite=%ld, wrote: %s, numRead=%ld, response=%s\n",
                      driverName,
                      functionName,
                      (long)numWrite,
                      msgToServer,
                      (long)numRead,
                      response);
        }

        // Update parameters/PVs
        updateTimeStamp();
        setStringParam(P_Server_PV_RBV, response);
        callParamCallbacks();
        unlock();

        // Temporization / time delay (in seconds), between each iteration of the current loop
        epicsThreadSleep(POLLER_PERIOD);
    }
}


//=================================================================================================
// EPICS boilerplate for registering 'asyndemoConfigure(portName') in IOC shell
//
#include <iocsh.h>
#include <epicsExport.h>
extern "C"
{

static const iocshArg initArg0 = { "portName", iocshArgString };
static const iocshArg initArg1 = { "IPPortName", iocshArgString };
static const iocshArg * const initArgs[] = { &initArg0, &initArg1 };
static const iocshFuncDef initFuncDef = {"asyndemoserverConfigure", 2, initArgs };
static void initCallFunc(const iocshArgBuf *args)
{
    new asyndemoserver(args[0].sval, args[1].sval);
}

void asyndemoserverRegister(void)
{
    iocshRegister(&initFuncDef, initCallFunc);
}

// See asyndemoInclude.dbd
epicsExportRegistrar(asyndemoserverRegister);

}

